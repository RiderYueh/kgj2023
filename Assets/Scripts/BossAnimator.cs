using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossAnimator : MonoBehaviour
{
    public Animator main_Animator;
    public Animator move_Animator;

    public string scemeName;

    private void Start()
    {
        if(scemeName == "S3")
        {
            main_Animator.SetTrigger("SH");
        }
        else if (scemeName == "S1")
        {
            
        }
        else
        {
            main_Animator.SetTrigger("G8");
        }

        if(move_Animator != null)
            move_Animator.enabled = false;
    }
}
