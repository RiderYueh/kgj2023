using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class NormalEnd : MonoBehaviour
{
    public Text m_Text;
    private void Start()
    {
        StartCoroutine(WaitTimeAction(5,()=> {
            GM.instance.isEnded = true;
            GM.instance.counted = 1033 + GM.instance.counted;
            GM.instance.OnLoadScene("Title");
        }));

        if(GM.instance.isEnded)
        {
            m_Text.text = "�A�O"+GM.instance.counted.ToString()+"�����w!!";
        }
    }
    private IEnumerator WaitTimeAction(float timer, UnityAction ua)
    {
        yield return new WaitForSeconds(timer);
        ua();
    }
}
