using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GM
{
    private static GM _instance;
    public static GM instance
    { 
        get
        {
            if (_instance != null) return _instance;
            else
            {
                _instance = new GM();
                return _instance;
            }
        }
    }

    public int hp;
    public bool isCanMove = false;
    public bool isS2Normal = false;
    public bool isS2Repeat = false;
    public bool isEnded = false;
    public int counted = 0;
    public bool isMouseLock = false;

    public void Init()
    {
        hp = 10;
    }

    public void OnLoadScene(string name)
    {
        SceneManager.LoadScene(name);
    }

    public void onExitGame()
    {
        Application.Quit();
    }
}
