using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityStandardAssets.Characters.FirstPerson;

public class SettingUI : MonoBehaviour
{
    public GameObject uiObj;
    public GameObject wifiIcon;
    public Button btnCloseWifi;
    public Button btnOpenWifi;
    public Button btnCloseMenu;
    public Image imgCloseWifi;
    public Image imgOpenWifi;
    public FirstPersonController player;

    public UnityAction<bool> wifiChangeEvent = null;

    private void Start()
    {
        wifiIcon.SetActive(false);
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            uiObj.SetActive(!uiObj.activeInHierarchy);
            AudioManager.instance.PlaySound(AudioManager.ESound.OpenUI);
            if(uiObj.activeInHierarchy)
            {
                GM.instance.isCanMove = false;
                GM.instance.isMouseLock = false;
                Application.targetFrameRate = -1;
                if(player)
                {
                    player.m_MouseLook.SetCursorLock(false);
                }
            }
            else
            {
                LagSet();
                GM.instance.isMouseLock = true;
            }
        }
    }

    public void OnWifiBtnClicked(bool b)
    {
        AudioManager.instance.PlaySound(AudioManager.ESound.OpenUI);
        if (!SceneManager.GetActiveScene().name.Equals("S2"))
        {
            return;
        }
        btnCloseWifi.interactable = !b;
        imgCloseWifi.enabled = b;
        btnOpenWifi.interactable = b;
        imgOpenWifi.enabled = !b;
        if(wifiIcon)
        {
            wifiIcon.SetActive(!b);
            wifiChangeEvent?.Invoke(b);
        }
    }

    public void OnCloseBtnClicked()
    {
        uiObj.SetActive(false);
        AudioManager.instance.PlaySound(AudioManager.ESound.OpenUI);
        GM.instance.isMouseLock = true;
        LagSet();
    }

    public void OpenWifiIcon()
    {
        wifiIcon.SetActive(true);
    }

    private void LagSet()
    {
        GM.instance.isCanMove = true;
        if(player)
        {
            player.m_MouseLook.SetCursorLock(true);
        }
        if(wifiIcon.activeInHierarchy && !GM.instance.isS2Normal && SceneManager.GetActiveScene().name.Equals("S2"))
        {
            Application.targetFrameRate = 3;
        }
    }
}