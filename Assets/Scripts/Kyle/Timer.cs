using UnityEngine;
using UnityEngine.Events;

public class Timer : MonoBehaviour
{
    public float timeNow = 0;
    public float timeTarget = 0;

    public bool timeUp = false;
    public bool timeCounting = false;

    public bool useUpdate = true;
    public bool useFixedUpdate = false;

    public UnityAction TimeUpCallBack = null;

    void Start()
    {

    }

    void Update()
    {
        if(useFixedUpdate || timeUp)
        {
            if(!useFixedUpdate)
            {
                timeCounting = false;
            }
            return;
        }

        if(timeNow < timeTarget)
        {
            timeCounting = true;
            timeNow += Time.deltaTime;
        }
        else
        {
            timeCounting = false;
            timeUp = true;
            TimeUpCallBack?.Invoke();
        }
    }

    void FixedUpdate()
    {
        if(useUpdate || timeUp)
        {
            if(!useUpdate)
            {
                timeCounting = false;
            }
            return;
        }

        if(timeNow < timeTarget)
        {
            timeCounting = true;
            timeNow += Time.fixedDeltaTime;
        }
        else
        {
            timeCounting = false;
            timeUp = true;
            TimeUpCallBack?.Invoke();
        }
    }

    public void SetTime(float sec, bool setUseFixedUpdate = false)
    {
        timeNow = 0;
        timeTarget = sec;
        timeUp = false;
        timeCounting = true;
        useUpdate = !setUseFixedUpdate;
        useFixedUpdate = setUseFixedUpdate;
    }
}