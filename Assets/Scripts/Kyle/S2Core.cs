using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using EnemyAI;
using UnityEngine.Events;

public class S2Core : MonoBehaviour
{
    public Timer timer;
    public GameObject player;
    public GameObject enemy;
    public GameObject aiDialogue;
    public Image imgAi;
    public Text scoreTxt;
    public Text remainTxt;
    public Text aiTxt;
    public Vector2 xRange;
    public Vector2 zRange;
    public int totalTime = 60;
    public int passConditionCount = 10;
    public float enemyStaySec = 2;
    public SettingUI sui;
    public Sprite spAINor;
    public Sprite spAIAng;

    private int remainTime = 60;
    private int hitCount = 0;
    private float delayCloseAiChatSec = 2;
    private float delayLoadSceneSec = 2;
    private EnemyHealth currEnemyHealth;
    private bool isGame = false;
    private bool isCanDect = false;

    public Animator ani_Boss;
    public Animator ani_KB;

    private void Start()
    {
        Application.targetFrameRate = -1;
        sui.wifiChangeEvent = (b) => {
            if(b)
            {
                OpenAiChat(S2TextContent.wifi2);
                AudioManager.instance.PlayTeach(true);
            }
        };

        if(GM.instance.isS2Normal)
        {
            imgAi.sprite = spAINor;
            aiTxt.text = S2TextContent.start1;
        }
        else
        {
            imgAi.sprite = spAIAng;
            if(GM.instance.isS2Repeat)
            {
                aiTxt.text = S2TextContent.start3;
            }
            else
            {
                aiTxt.text = S2TextContent.start2;
            }
            
            GM.instance.isS2Repeat = true;
        }

        timer.TimeUpCallBack = StartGame;
        timer.SetTime(2);
        GM.instance.isCanMove = false;
        aiDialogue.SetActive(true);
        
        AudioManager.instance.PlayTeach(false);

    }

    private void Update()
    {
        if(isCanDect)
        {
            if(currEnemyHealth.health <= 0)
            {
                isCanDect = false;
                hitCount += 1;
                scoreTxt.text = hitCount.ToString();
                Debug.Log("HIT +1");
                if(hitCount == 2 && !GM.instance.isS2Normal)
                {
                    OpenAiChat(S2TextContent.wifi1);
                    AudioManager.instance.PlayTeach(true);
                    StartCoroutine(WaitTimeAction(1.5f , ()=> {
                        ani_Boss.enabled = true;
                        
                        StartCoroutine(WaitTimeAction(3, () => { 
                            sui.OpenWifiIcon(); 
                            Application.targetFrameRate = 3; 
                        }));
                        StartCoroutine(WaitTimeAction(1f, () => { ani_KB.enabled = true; }));
                    }));
                }
            }
        }
    }

    private IEnumerator WaitTimeAction(float timer, UnityAction ua)
    {
        yield return new WaitForSeconds(timer);
        ua();
    }

    public void StartGame()
    {
        aiDialogue.SetActive(false);
        GM.instance.isCanMove = true;
        hitCount = 0;
        scoreTxt.text = hitCount.ToString();
        remainTime = totalTime;
        remainTxt.text = remainTime.ToString();
        timer.TimeUpCallBack = CountTime;
        timer.SetTime(1);
        isGame = true;
        StartCoroutine(NextTurn());
    }

    private void CountTime()
    {
        remainTime--;
        remainTxt.text = remainTime.ToString();
        if(remainTime == 0)
        {
            EndGame();
        }
        else
        {
            timer.SetTime(1);
        }
    }

    private void EndGame()
    {
        isGame = false;
        GM.instance.isCanMove = false;
        aiDialogue.SetActive(true);
        if(hitCount >= passConditionCount)
        {
            Debug.Log("YOU WIN");
            if(GM.instance.isS2Normal)
            {
                aiTxt.text = S2TextContent.end1;
                AudioManager.instance.PlayTeach(false);
                timer.TimeUpCallBack = () => {
                    GM.instance.OnLoadScene("NormalEnd");
                };
            }
            else
            {
                aiTxt.text = S2TextContent.end2;
                AudioManager.instance.PlayTeach(true);
                timer.TimeUpCallBack = () => {
                    GM.instance.OnLoadScene("S3");
                };
            }
        }
        else
        {
            Debug.Log("YOU LOSE");
            aiTxt.text = S2TextContent.lose;
            timer.TimeUpCallBack = () => {
                GM.instance.OnLoadScene("S2");
            };
        }
        timer.SetTime(delayLoadSceneSec);
    }

    private IEnumerator NextTurn()
    {
        CreateEnemy();
        isCanDect = true;
        yield return new WaitForSeconds(enemyStaySec);
        Destroy(currEnemyHealth.gameObject);
        if(isGame)
        {
            StartCoroutine(NextTurn());
        }
    }

    private void CreateEnemy()
    {
        float x = Random.Range(xRange.x, xRange.y);
        float z = Random.Range(zRange.x, zRange.y);
        Vector3 pos = new Vector3(x, 0, z);
        GameObject go = Instantiate(enemy);
        go.transform.localPosition = pos;
        currEnemyHealth = go.GetComponent<EnemyHealth>();
        go.SetActive(true);
    }

    private void OpenAiChat(string text)
    {
        if(IsInvoking(nameof(CloseAiChat)))
        {
            CancelInvoke(nameof(CloseAiChat));
        }
        aiDialogue.SetActive(true);
        aiTxt.text = text;
        Invoke(nameof(CloseAiChat), delayCloseAiChatSec);
    }

    private void CloseAiChat()
    {
        aiDialogue.SetActive(false);
    }
}
