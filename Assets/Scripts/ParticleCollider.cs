using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleCollider : MonoBehaviour
{
    public int particleNum;
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            if(Teach.instance != null)
            {
                Teach.instance.OnHitCollider(particleNum);
            }
        }
    }
}
