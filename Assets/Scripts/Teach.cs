using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class Teach : MonoBehaviour
{
    static public Teach instance;
    public Text text_mainTeach;
    public GameObject go_allTeach;
    public GameObject go_Teacher;
    public GameObject go_p1; //圖片1
    public GameObject go_p2; //圖片2
    public GameObject go_p3; //圖片3

    public GameObject go_particle01;
    public GameObject go_particle02;
    public GameObject go_particle03;
    public GameObject go_particle04;

    public GameObject go_player;
    public Sprite angrySpirte;
    public Image img_Teacher;

    public GameObject go_Boss1;
    public GameObject go_Boss2;

    public Animator ani_Boss2;

    private int count = 0;
    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        go_allTeach.SetActive(false);
        StartCoroutine(WaitTimeAction(1, F1));
        StartCoroutine(WaitTimeAction(4, F1_2));
        StartCoroutine(WaitTimeAction(7, F1_3));
        StartCoroutine(WaitTimeAction(10, F1_4));

        StartCoroutine(WaitTimeAction(12, F2));
        StartCoroutine(WaitTimeAction(14, F3));

    }

    void F1()
    {
        go_allTeach.SetActive(true);
        go_p1.SetActive(false);
        go_Teacher.SetActive(true);

        string tempStr = "歡迎來到Cpex Legacy!!";
        if (GM.instance.isEnded)
        {
            tempStr = "歡迎來到Cpex Legacy,\n我今天運氣真好,嘿嘿!!";
        }
        text_mainTeach.text = tempStr;
        AudioManager.instance.PlayTeach(false);
    }
    void F1_2()
    {
        go_allTeach.SetActive(true);
        go_p1.SetActive(false);
        go_Teacher.SetActive(true);
        string tempStr = "請問你是學生嗎? ";
        if(GM.instance.isEnded)
        {
            tempStr = "請注意不要亂跑上山喔";
        }
        text_mainTeach.text = tempStr;
        AudioManager.instance.PlayTeach(false);
    }
    void F1_3()
    {
        go_allTeach.SetActive(true);
        go_p1.SetActive(false);
        go_Teacher.SetActive(true);
        string tempStr = "痾ㄏ痾ㄏ痾ㄏ ";
        if (GM.instance.isEnded)
        {
            tempStr = "沒事啦 , 痾ㄏ痾ㄏ痾ㄏ ";
        }
        text_mainTeach.text = tempStr;
        AudioManager.instance.PlayTeach(false);
    }
    void F1_4()
    {
        go_allTeach.SetActive(true);
        go_p1.SetActive(false);
        go_Teacher.SetActive(true);
        string tempStr = "我是你的導師,Ed Ward";
        if (GM.instance.isEnded)
        {
            tempStr = "我是你的導師,Ed Ward,\n來 我給你加個Line";
        }
        text_mainTeach.text = tempStr;
        AudioManager.instance.PlayTeach(false);
    }

    void F2()
    {
        
        go_p1.SetActive(false);
        go_Teacher.SetActive(true);
        string tempStr = "讓我們來練習基本操作吧！";
        if (count == 2) tempStr = "來練習基本操作";
        text_mainTeach.text = tempStr;
        AudioManager.instance.PlayTeach(false);

        count += 1;
    }

    void F3()
    {
        go_p1.SetActive(true);
        go_particle01.SetActive(true);
        string tempStr = "前往光圈";
        text_mainTeach.text = tempStr;
        GM.instance.isCanMove = true;
    }

    void F4()
    {
        go_Teacher.SetActive(true);
        string tempStr = "好，然後來練習跳躍...";
        if(count == 2) tempStr = "去練習跳躍...";
        text_mainTeach.text = tempStr;
        AudioManager.instance.PlayTeach(false);
        go_p2.SetActive(true);
        go_particle02.SetActive(true);
    }

    void F5()
    {
        go_Teacher.SetActive(true);
        string tempStr = "嗯...跳得還行，接下來請練習蹲下";
        if (count == 2) tempStr = "去練習蹲下...";
        text_mainTeach.text = tempStr;
        AudioManager.instance.PlayTeach(false);
        go_p3.SetActive(true);
        go_particle03.SetActive(true);
    }

    void F6()
    {
        go_Teacher.SetActive(true);
        string tempStr = "嗯...蹲得有點醜";
        if (count == 2) tempStr = "還是蹲得很醜";
        text_mainTeach.text = tempStr;
        AudioManager.instance.PlayTeach(false);

        go_particle04.SetActive(true);
    
    }


    void F3End()
    {
        OnReset();
        StartCoroutine(WaitTimeAction(0.3f, F4));
    }

    void F4End()
    {
        OnReset();
        StartCoroutine(WaitTimeAction(0.3f, F5));
    }

    void F5End()
    {
        OnReset();
        F6();
    }

    void F6End()
    {
        OnReset();
        go_Teacher.SetActive(true);
        string tempStr = "你得再練習一次";
        if (count == 2) tempStr = "這操作怎麼吃雞? 再去練習";
        text_mainTeach.text = tempStr;
        AudioManager.instance.PlayTeach(false);

        StartCoroutine(WaitTimeAction(0.5f, () => {
            go_player.GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().enabled = false;
            go_player.transform.position = new Vector3(2.6f, 1.43f, -5.3f);
            StartCoroutine(WaitTimeAction(0.1f, () => {
                go_player.GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().enabled = true;
            }));
        }));

        StartCoroutine(WaitTimeAction(1, F2));
        StartCoroutine(WaitTimeAction(2, F3));
    }

    bool isF10 = false;
    void F10()
    {
        if (isF10) return;
        isF10 = true;

        img_Teacher.sprite = angrySpirte;
        AudioManager.instance.PlayTeach(true);
        go_Teacher.SetActive(true);
        go_Boss1.SetActive(false);
        go_Boss2.SetActive(true);
    }

    bool isF11 = false;
    void F11()
    {
        if (isF11) return;
        isF11 = true;

        StartCoroutine(WaitTimeAction(4, ()=> {
            OnReset();
            AudioManager.instance.PlaySound(AudioManager.ESound.Gun);
            StartCoroutine(WaitTimeAction(0.1f, () => {
                AudioManager.instance.PlaySound(AudioManager.ESound.Gun);
            }));
            StartCoroutine(WaitTimeAction(0.2f, () => {
                AudioManager.instance.PlaySound(AudioManager.ESound.Gun);
            }));
            StartCoroutine(WaitTimeAction(0.3f, () => {
                AudioManager.instance.PlaySound(AudioManager.ESound.Gun);
            }));
            StartCoroutine(WaitTimeAction(0.4f, () => {
                AudioManager.instance.PlaySound(AudioManager.ESound.Gun);
            }));
            StartCoroutine(WaitTimeAction(0.5f, () => {
                AudioManager.instance.PlaySound(AudioManager.ESound.Gun);
            }));
            AudioManager.instance.PlaySound(AudioManager.ESound.HeadShot);
            //go_Boss2.SetActive(false);
            ani_Boss2.enabled = true;
            go_allTeach.SetActive(false);
        }));
    }

    bool isF12 = false;
    void F12()
    {
        if (isF12) return;
        isF12 = true;

        OnReset();
        AudioManager.instance.PlayTeach(false);

        string tempStr = "又一個不想聽我講話的學生!!";
        text_mainTeach.text = tempStr;

    }

    void OnReset()
    {
        string tempStr = "";
        text_mainTeach.text = tempStr;

        go_p2.SetActive(false);
        go_particle02.SetActive(false);
        go_p1.SetActive(false);
        go_particle01.SetActive(false);
        go_p3.SetActive(false);
        go_particle03.SetActive(false);
        go_particle04.SetActive(false);
    }

    public void OnHitCollider(int num)
    {
        if(num == 1)
        {
            F3End();
            AudioManager.instance.PlaySound(AudioManager.ESound.GetParticle);
        }

        else if (num == 2)
        {
            F4End();
            AudioManager.instance.PlaySound(AudioManager.ESound.GetParticle);
        }

        else if (num == 3)
        {
            F5End();
            AudioManager.instance.PlaySound(AudioManager.ESound.GetParticle);
        }

        else if(num == 4)
        {
            F6End();
            AudioManager.instance.PlaySound(AudioManager.ESound.GetParticle);
        }

        else if (num == 109)
        {
            F11();
        }

        else if (num == 110)
        {
            F10();
        }

        else if (num == 11)
        {
            F12();
        }

        else if(num == 111)
        {
            GM.instance.isS2Normal = true;
            GM.instance.OnLoadScene("S2");
            
        }
        else if (num == 112)
        {
            GM.instance.isS2Normal = false;
            GM.instance.OnLoadScene("S2");
        }
    }

    private IEnumerator WaitTimeAction(float timer, UnityAction ua)
    {
        yield return new WaitForSeconds(timer);
        ua();
    }
}
