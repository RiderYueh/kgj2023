using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
public class Final : MonoBehaviour
{
    public float timeDelay = 5f;
    public float timeDuration = 2f;
    public bool isTrans = false;
    public Image BG;
    public AudioClip shot;
    public AudioSource audios;
    public float targetAngle;
    public bool isRot = false;
    public GameObject exitUI;
    public GameObject go_gun;
    public GameObject go_cross;
    void Start()
    {
        StartCoroutine(waitForTimer(timeDelay, () => {
            isTrans = true;
            go_gun.SetActive(true);
            go_cross.SetActive(true);
        }
        ));
        StartCoroutine(waitForTimer(timeDelay+timeDuration+1, () => {
            AudioManager.instance.PlaySound(AudioManager.ESound.Gun);

            StartCoroutine(waitForTimer(0.1f, () => {
                AudioManager.instance.PlaySound(AudioManager.ESound.Gun);
            }));
            StartCoroutine(waitForTimer(0.2f, () => {
                AudioManager.instance.PlaySound(AudioManager.ESound.Gun);
            }));
            StartCoroutine(waitForTimer(0.3f, () => {
                AudioManager.instance.PlaySound(AudioManager.ESound.Gun);
            }));
            StartCoroutine(waitForTimer(0.4f, () => {
                AudioManager.instance.PlaySound(AudioManager.ESound.Gun);
            }));
            StartCoroutine(waitForTimer(0.5f, () => {
                AudioManager.instance.PlaySound(AudioManager.ESound.Gun);
            }));
        }
        ));
        StartCoroutine(waitForTimer(2f, () => {
            isRot = true;
        }
        ));
        StartCoroutine(waitForTimer(timeDelay + timeDuration + 2, () => {
            exitUI.SetActive(true);
            go_cross.SetActive(false);
        }
        ));
    }

    // Update is called once per frame
    void Update()
    {
        if (isTrans)
        {
            if (BG.color.a<1)
            {
                BG.color = new Color(0, 0, 0, BG.color.a + Time.deltaTime / timeDuration);
            }
        }
        if (isRot)
        {
            transform.rotation = Quaternion.Euler(
            new Vector3(
                0, transform.rotation.eulerAngles.y - 0.5f*Mathf.Abs(transform.rotation.eulerAngles.y - targetAngle) * Time.deltaTime, 0
            ));
        }
        

    }

    IEnumerator waitForTimer(float t,UnityAction a)
    {
        yield return new WaitForSeconds(t);
        a();
    }


}
