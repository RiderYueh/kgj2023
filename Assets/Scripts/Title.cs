using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class Title : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        AudioManager.instance.PlayBGM(AudioManager.EBGM.BGM);

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void onLoadScene(string name)
    {
        //SceneManager.LoadScene(name);
        SceneManager.LoadSceneAsync(name);
    }

    public void onExitGame()
    {
        Application.Quit();
    }
}
