using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using EnemyAI;

public class S3 : MonoBehaviour
{
    public static S3 instance;
    public GameObject go_player;
    public List<GameObject> list_hidePlace;

    public GameObject go_KB;
    public Image img_ctrl;
    public Image img_z;
    public Image img_Hand;
    public GameObject go_boss;

    public List<EnemyHealth> enemyPrefabs;
    public GameObject go_Text;
    public GameObject go_Hit;
    public GameObject go_lost;
    public Animator boss_Ani;

    public GameObject go_End;
    public GameObject go_Text01;
    public GameObject go_Text02;
    public GameObject go_Text03;

    public GameObject go_bg2;
    public GameObject go_vic;

    private class Enemy
    {
        public bool isToDel = false;
        public EnemyHealth obj;
    }
    private List<Enemy> enemyObjs = new List<Enemy>();

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        AudioManager.instance.PlayTeach(true);
        AudioManager.instance.PlayBGM(AudioManager.EBGM.BossBGM);

        go_KB.SetActive(false);
        GM.instance.isCanMove = true;

        StartCoroutine(WaitTimeAction(3, ()=> {
            go_Text02.SetActive(true);
            go_Text01.SetActive(false);
            AudioManager.instance.PlayTeach(true);
        }));

        StartCoroutine(WaitTimeAction(5, () => {
            go_Text03.SetActive(true);
            go_Text02.SetActive(false);
            AudioManager.instance.PlayTeach(true);
        }));

        StartCoroutine(WaitTimeAction(2, CreateEnemy));
        StartCoroutine(WaitTimeAction(7, OpenKB));
    }

    float timer = 0;
    bool isStart = false;
    bool isBG2 = false;
    private void Update()
    {
        if(isStart)
        {
            timer -= Time.deltaTime;
            if (timer <= 0)
            {
                timer = 0.5f;
                DelHidePlace();
            }
        }

        if(Input.GetKey(KeyCode.LeftControl))
        {
            if(Input.GetKeyDown(KeyCode.Y))
            {
                DelHidePlace(true);
            }
            if (Input.GetKeyDown(KeyCode.Z))
            {
                if(!isBG2)
                {
                    isBG2 = true;
                    go_bg2.SetActive(true);
                    AudioManager.instance.PlayTeach(true);
                    StartCoroutine(WaitTimeAction(2,()=> { go_bg2.SetActive(false); }));
                }
                DelHidePlace(false);
            }
        }

        for(int i = 0; i < enemyObjs.Count; i++)
        {
            if(enemyObjs[i].obj.health <= 0)
            {
                if (enemyObjs[i].isToDel) continue;

                enemyObjs[i].isToDel = true;
                int id = i;
                StartCoroutine(WaitTimeAction(0.5f, ()=> {
                    enemyObjs[id].obj.gameObject.SetActive(false);
                }));
                

                StartCoroutine(WaitTimeAction(1, () =>
                {
                    
                    Destroy(enemyObjs[id].obj.gameObject , 0.1f);
                    EnemyHealth go = Instantiate(enemyPrefabs[id]);
                    go.gameObject.SetActive(true);
                    enemyObjs[id].obj = go;
                    enemyObjs[id].isToDel = false;
                }));
            }
        }
    }

    

    private void OpenKB()
    {
        go_KB.SetActive(true);
        isStart = true;
        go_Text.SetActive(false);
        
    }

    bool isHit = false;
    public void OnHit()
    {
        if (isHit) return;
        go_Hit.SetActive(true);

        StartCoroutine(WaitTimeAction(0.1f,()=> {
            isHit = false;
            go_Hit.SetActive(false);
        }));

    }

    private void CreateEnemy()
    {
        for(int i = 0; i < enemyPrefabs.Count; i++)
        {
            int id = i;
            EnemyHealth go = Instantiate(enemyPrefabs[id]);
            go.gameObject.SetActive(true);
            Enemy eData = new Enemy() { isToDel = false, obj = go };
            enemyObjs.Add(eData);
        }
        boss_Ani.enabled = true;
    }

    private void DelHidePlace(bool isRevert = false)
    {
        if(isRevert)
        {
            foreach (GameObject go in list_hidePlace)
            {
                if (!go.activeSelf)
                {
                    AudioManager.instance.PlaySound(AudioManager.ESound.OpenShield);
                    go.SetActive(true);
                    break;
                }
            }
            return;
        }



        bool isStillHide = false;
        foreach (GameObject go in list_hidePlace)
        {
            if (go.activeSelf)
            {
                isStillHide = true;
            }
        }

        if (!isStillHide) return;


        img_ctrl.color = Color.gray;
        img_z.color = Color.gray;
        img_Hand.color = Color.gray;

        StartCoroutine(WaitTimeAction(0.2f, RevertKB));
        foreach (GameObject go in list_hidePlace)
        {
            if (go.activeSelf)
            {
                go.SetActive(false);
                AudioManager.instance.PlaySound(AudioManager.ESound.CloseShield);
                break;
            }
        }
    }

    private void RevertKB()
    {
        img_ctrl.color = Color.white;
        img_z.color = Color.white;
        img_Hand.color = Color.white;
    }

    private IEnumerator WaitTimeAction(float timer, UnityAction ua)
    {
        yield return new WaitForSeconds(timer);
        ua();
    }

    public void PlayerDie()
    {
        go_lost.SetActive(true);
        StartCoroutine(WaitTimeAction(2, ()=> { GM.instance.OnLoadScene("S3"); }));
    }

    bool isBossDie = false;
    public void BossDie()
    {
        if (isBossDie) return;
        isBossDie = true;

        boss_Ani.enabled = false;
        isStart = false;
        for (int i = 0; i < enemyObjs.Count; i++)
        {
            enemyObjs[i].obj.gameObject.SetActive(false);
        }

        go_vic.SetActive(true);

        StartCoroutine(WaitTimeAction(2, () => {
            go_End.SetActive(true);
            
            StartCoroutine(WaitTimeAction(2, () => {
                GM.instance.OnLoadScene("Final");
            }));
        }));

    }
}
