using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AudioManager : MonoBehaviour
{
    public static AudioManager instance;
    public List<AudioStruct> list_AudioStruct;
    public List<SoundStruct> list_SoundStruct;
    public List<LoopStruct> list_LoopStruct;
    public AudioSource mainAudioSource;

    public AudioSource trackLoop01;




    public enum EBGM
    {
        BGM,
        BossBGM,
    }

    public enum ESound
    {
        TeachN0,
        TeachN1,
        TeachN2,
        TeachN3,
        TeachN4,
        TeachA1,
        TeachA2,
        TeachA3,
        TeachA4,
        TeachA5,

        HeadShot,
        Gun,
        GetParticle,
        OpenUI,
        DelCube,
        OpenShield,
        CloseShield,
    }

    public enum eLoop
    {

    }

    public void PlayTeach(bool isAngry)
    {
        int ran = UnityEngine.Random.Range(0,5);
        if (!isAngry)
        {
            if(ran == 0) PlaySound(ESound.TeachN0);
            else if (ran == 1) PlaySound(ESound.TeachN1);
            else if (ran == 2) PlaySound(ESound.TeachN2);
            else if (ran == 3) PlaySound(ESound.TeachN3);
            else if (ran == 4) PlaySound(ESound.TeachN4);
        }
        else
        {
            if (ran == 0) PlaySound(ESound.TeachA1);
            else if (ran == 1) PlaySound(ESound.TeachA2);
            else if (ran == 2) PlaySound(ESound.TeachA3);
            else if (ran == 3) PlaySound(ESound.TeachA4);
            else if (ran == 4) PlaySound(ESound.TeachA5);
        }
    }


    [Serializable]
    public struct AudioStruct
    {
        public EBGM eBGM;
        public AudioClip clip;
    }

    [Serializable]
    public struct SoundStruct
    {
        public ESound eSound;
        public AudioClip clip;
    }

    [Serializable]
    public struct LoopStruct
    {
        public eLoop eLoop;
        public AudioClip clip;
    }

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        PlayBGM(EBGM.BGM);
    }

    private void Update()
    {
        if(Input.GetKeyDown("="))
        {
            Time.timeScale += 0.5f;
        }
        if (Input.GetKeyDown("-"))
        {
            Time.timeScale = 1;
        }
    }

    public void PlaySound(ESound eSound , float volume = 1)
    {
        foreach (SoundStruct tempAudioStruct in list_SoundStruct)
        {
            if (tempAudioStruct.eSound == eSound)
            {
                mainAudioSource.PlayOneShot(tempAudioStruct.clip, volume);
                break;
            }
        }
    }

    public void PlayBGM(EBGM eSound)
    {
        foreach (AudioStruct tempAudioStruct in list_AudioStruct)
        {
            if (tempAudioStruct.eBGM == eSound)
            {
                mainAudioSource.clip = tempAudioStruct.clip;
                mainAudioSource.loop = true;
                trackLoop01.volume = 0.3f;
                mainAudioSource.volume = 0.3f;
                mainAudioSource.Play();
                break;
            }
        }
    }

    public void PlayLoop(eLoop eLoop, float volume = 0.3f)
    {
        foreach (LoopStruct tempAudioStruct in list_LoopStruct)
        {
            if (tempAudioStruct.eLoop == eLoop)
            {
                trackLoop01.clip = tempAudioStruct.clip;
                trackLoop01.clip = tempAudioStruct.clip;
                trackLoop01.volume = 0.3f;
                trackLoop01.loop = true;
                trackLoop01.Play();
                break;
            }
        }
    }

    private IEnumerator WaitTimeAction(float timer, UnityAction ua)
    {
        yield return new WaitForSeconds(timer);
        ua();
    }
}
